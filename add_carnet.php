<?php
require_once 'php/session.php';
require_once 'libs/autoload.php';
require_once 'libs/funciones.php';

$bi = new Bibliotecas();
$lista_bib = json_decode($bi->listarBibliotecas());

$c_ultimo = new Carnets();
$ultimo = $c_ultimo->ultimoCarnet();
$fecha_exp = time();
$fecha_ven = $fecha_exp + 31556926;
$fecha_exp_larga = date('d/m/Y', $fecha_exp);
$fecha_ven_larga = date('d/m/Y', $fecha_ven);
$fecha_exp_corta = date('m/y', $fecha_exp);
$fecha_ven_corta = date('m/y', $fecha_ven);
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Crear carnet</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/select2/select2.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/styles.css">
	</head>
	<body class="sidebar-mini skin-red-light">
		<div class="wrapper">
			<div id="logoHead" style="text-align: center;background-color: #fff;">
				<img src="dist/img/Logo2.jpg" width="850" alt="">
			</div>
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar" style="top: 98px;">
				<?php include 'inc/main-aside.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Carnets <small>Crear carnet</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Carnets</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Ingrese los siguientes datos</h3>
								</div>
								<form action="php/carnets.php" method="POST" autocomplete="off">
									<div class="box-body">
										<div class="form-group">
											<label for="cedula">Cédula de Identidad</label>
											<input type="text" id="cedula" class="form-control" placeholder="Ejm: V20857323">
											<br />
											<div id="alertaCedula">
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="nombre">Nombre</label>
													<input type="text" id="nombre" class="form-control" placeholder="Ejm: Pedro">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="apellido">Apellido</label>
													<input type="text" id="apellido" class="form-control" placeholder="Ejm: Pérez">
												</div>
											</div>
										</div>
										<?php if ($_SESSION['nivel'] == 1) { ?>
										<div class="form-group">
											<label for="biblioteca">Biblioteca</label>
											<select name="biblioteca" id="biblioteca" class="form-control select2" style="width: 100%;">
												<option value="<?php echo $_SESSION['biblioteca']; ?>"><?php echo biblioteca($_SESSION['biblioteca']); ?></option>
												<?php for ($i=0; $i < count($lista_bib); $i++) { ?>
												<option value="<?php echo $lista_bib[$i]->id; ?>"><?php echo $lista_bib[$i]->biblioteca; ?></option>
												<?php } ?>
											</select>
										</div>
										<?php } ?>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="fecha_exp">Fecha de expedición</label>
													<input type="text" id="fecha_exp" value="<?php echo $fecha_exp_larga; ?>" readonly class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="fecha_ven">Fecha de vencimiento</label>
													<input type="text" id="fecha_ven" value="<?php echo $fecha_ven_larga; ?>" readonly class="form-control">
												</div>
											</div>
										</div>
										<div id="alertas">
											
										</div>
									</div>
									<div class="box-footer">
										<button type="button" id="agregar" class="btn btn-primary btn-flat">Crear carnet</button>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Vista previa</h3>
								</div>
								<div id="preview" class="box-body">
									<img src="php/img_carnet.php" alt="" class="img-responsive">
								</div>
								<div class="box-footer">
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.js"></script>
		<script type="text/javascript" src="dist/js/carnets.js"></script>
		<script type="text/javascript">
		$(function() {
			$(".select2").select2();
		});
		</script>
	</body>
</html>