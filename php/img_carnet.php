<?php
header("Content-type: image/jpeg");
session_start();
require_once '../libs/Bibliotecas.php';
$bib = new Bibliotecas();

$biblioteca = "B. P. Biblioteca";
$codigo = "0-0";
$cedula = "V12345678";
$nombre = "Apellido Nombre";

if (!empty($_GET['carnet']) || !empty($_GET['cedula']) || !empty($_GET['nombre']) || !empty($_GET['apellido'])) {

	if (!empty($_GET['biblioteca'])) {
		$dataBibilioteca = json_decode($bib->read($_GET['biblioteca']));
	} elseif (isset($_SESSION['biblioteca'])) {
		$dataBibilioteca = json_decode($bib->read($_SESSION['biblioteca']));
	}
	
	$biblioteca = $dataBibilioteca->biblioteca;
	$codigo = $_GET['carnet'];
	$cedula = $_GET['cedula'];
	$nombre = $_GET['apellido'] . ' ' . $_GET['nombre'];
}

$fuente = '../dist/fonts/arialbd.ttf';

$im = imagecreatefromjpeg("../dist/img/Carnet1.jpg");

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 7 * strlen($biblioteca)) / 2;

imagettftext($im, 10, 0, 60, 15, $negro, $fuente, $biblioteca);
//imagestring($im, 3, $px, 5, $biblioteca, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 10 * strlen($codigo)) / 2;
imagettftext($im, 12, 0, 150, 30, $negro, $fuente, $codigo);
//imagestring($im, 5, $px, 18, $codigo, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 1 * strlen($cedula)) / 4;
imagettftext($im, 12, 0, 60, 120, $negro, $fuente, $cedula);
//imagestring($im, 5, $px, 109, $cedula, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 8.6 * strlen($nombre)) / 5;
imagettftext($im, 12, 0, 60, 168, $negro, $fuente, $nombre);
//imagestring($im, 5, $px, 155, $nombre, $negro);

imagejpeg($im);
imagedestroy($im);
?>