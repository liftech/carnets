<?php
session_start();
if (!isset($_SESSION['id']) || !isset($_SESSION['nombre']) || !isset($_SESSION['vida'])) {
	session_destroy();
	header('Location: index.php');
} else {
	if (time() > $_SESSION['vida']) {
		session_destroy();
		header('Location: index.php');
	} else {
		$_SESSION['vida'] = time() + 300;
	}
}
?>