<?php
header("Content-type: image/jpeg");

$fecha_exp = "01/01/2001";
$fecha_ven = "01/01/2002";

if (!empty($_GET['fecha_exp']) || !empty($_GET['fecha_ven'])) {
	
	$fecha_exp = date('d/m/Y', $_GET['fecha_exp']);
	$fecha_ven = date('d/m/Y', $_GET['fecha_ven']);
}

$fuente = '../dist/fonts/arialbd.ttf';

$im = imagecreatefromjpeg("../dist/img/Carnet2.jpg");

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 0.8 * strlen($fecha_exp)) / 3;
imagettftext($im, 7, 0, 90, 145, $negro, $fuente, $fecha_exp);
//imagestring($im, 1, $px, 136, $fecha_exp, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 0.8 * strlen($fecha_ven)) / 1.2;
imagettftext($im, 7, 0, 230, 145, $negro, $fuente, $fecha_ven);
//imagestring($im, 1, $px, 136, $fecha_ven, $negro);

imagejpeg($im);
imagedestroy($im);
?>