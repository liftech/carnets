<?php
if (isset($_FILES['archivo'])) {
	if ($_FILES['archivo']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
		session_start();

		require_once '../libs/Personas.php';
		require_once '../libs/Carnets.php';
		require_once '../libs/Bibliotecas.php';

		$json = array();
		$exitos = array();
		$errores = array();

		$personas = new Personas();
		$carnets = new Carnets();
		$bibliotecas = new Bibliotecas();

		require_once '../libs/PHPExcel/IOFactory.php';
		$inputFileName = $_FILES['archivo']['tmp_name'];
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

		for ($i=1; $i <= count($sheetData) ; $i++) {	
			if (!is_null($sheetData[$i]['A']) || !is_null($sheetData[$i]['B']) || !is_null($sheetData[$i]['C']) || !is_null($sheetData[$i]['D'])) {
				$dataPersonas = json_decode($personas->read($sheetData[$i]['A']));
				if ($dataPersonas->id == NULL) {
					$dataBiblioteca = json_decode($bibliotecas->buscarPor($sheetData[$i]['D']));

					if ($dataBiblioteca->registros > 0) {
						$createPersona = json_decode($personas->create($sheetData[$i]['A'], $sheetData[$i]['B'], $sheetData[$i]['C']));
						$result = json_decode($carnets->create($dataBiblioteca->id, $createPersona->id, $_SESSION['id']));

						array_push($exitos, array(
							"cedula" => $sheetData[$i]['A'],
							"nombre" => $sheetData[$i]['B'],
							"apellido" => $sheetData[$i]['C']
							));
					} else {
						array_push($errores, array(
							"cedula" => $sheetData[$i]['A'],
							"nombre" => $sheetData[$i]['B'],
							"apellido" => $sheetData[$i]['C'],
							"errors" => $dataBiblioteca->description
							));
					}
				} else {
					$dataCarnet = json_decode($carnets->read($dataPersonas->id));
					if ($dataCarnet->id == NULL) {
						$dataBiblioteca = json_decode($bibliotecas->buscarPor($sheetData[$i]['D']));

						if ($dataBiblioteca->registros > 0) {
							$createPersona = json_decode($personas->create($sheetData[$i]['A'], $sheetData[$i]['B'], $sheetData[$i]['C']));
							$result = json_decode($carnets->create($dataBiblioteca->id, $createPersona->id, $_SESSION['id']));

							array_push($exitos, array(
								"cedula" => $sheetData[$i]['A'],
								"nombre" => $sheetData[$i]['B'],
								"apellido" => $sheetData[$i]['C']
								));
						} else {
							array_push($errores, array(
								"cedula" => $sheetData[$i]['A'],
								"nombre" => $sheetData[$i]['B'],
								"apellido" => $sheetData[$i]['C'],
								"errors" => $dataBiblioteca->description
								));
						}
					} else {
						array_push($errores, array(
							"cedula" => $sheetData[$i]['A'],
							"nombre" => $sheetData[$i]['B'],
							"apellido" => $sheetData[$i]['C'],
							"errors" => "ya posee carnet"
							));
					}
				}
			}
		}

		$json = array(
			'exitos' => $exitos,
			'errores' => $errores
			);

		echo json_encode($json);
	}
}
?>