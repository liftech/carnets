<?php
session_start();
$patron = '/(v|V|e|E)([0-9]{7,10})/';

$json = array();

if (!empty($_POST['accion'])) {
	switch ($_POST['accion']) {
		case 'consulta':
			if (!empty($_POST['cedula'])) {
				preg_match($patron, $_POST['cedula'], $result);
				if (!empty($result)) {
					require_once '../libs/Personas.php';
					require_once '../libs/Carnets.php';
					$personas = new Personas();
					$carnets = new Carnets();
					$dataPersona = json_decode($personas->read($_POST['cedula']));
					if ($dataPersona->id == NULL) {
						$json = array(
							'status' => 1,
							'description' => 'la cédula no existe en la base de datos!'
							);
					} else {
						$dataCarnet = json_decode($carnets->read($dataPersona->id));
						if ($dataCarnet->id == NULL) {
							$json = array(
								'status' => 1,
								'description' => 'el carnet no existe en la base de datos!'
								);
						} else {
							$json = array(
								'status' => 0,
								'description' => 'esta persona ya posee un carnet!'
								);
						}
					}
				} else {
					$json = array(
						'status' => 3,
						'description' => 'la cédula no es un formato valido! (V12345678 o E12345678)'
						);
				}
			}

			echo json_encode($json);
			break;
		case 'agregar':
			if (!empty($_POST['cedula']) && !empty($_POST['nombre']) && !empty($_POST['apellido'])) {
				require_once '../libs/Personas.php';
				require_once '../libs/Carnets.php';
				$personas = new Personas();
				$carnets = new Carnets();
				$biblioteca = $_SESSION['biblioteca'];
				if (!empty($_POST['biblioteca']) || isset($_POST['biblioteca'])) {
					$biblioteca = $_POST['biblioteca'];
				}
				$dataPersona = json_decode($personas->create($_POST['cedula'], $_POST['nombre'], $_POST['apellido']));
				echo $carnets->create($biblioteca, $dataPersona->id, $_SESSION['id']);
			}
			break;
		case 'activar':
			if (!empty($_POST['id'])) {
				require_once '../libs/Carnets.php';
				$carnets = new Carnets();
				echo $carnets->activar($_POST['id']);
			}
			break;
	}
}
?>