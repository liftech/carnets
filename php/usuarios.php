<?php
$json = array();

if (!empty($_POST['nombre']) || !empty($_POST['apellido']) || !empty($_POST['biblioteca']) || !empty($_POST['nivel']) || !empty($_POST['usuario']) || !empty($_POST['clave']) || !empty($_POST['clave_r'])) {
	if ($_POST['clave'] == $_POST['clave_r']) {
		require_once '../libs/Usuarios.php';
		$usuarios = new Usuarios();
		$validar = json_decode($usuarios->read($_POST['usuario']));
		if (empty($validar->usuario)) {

			if (isset($_FILES['foto'])) {
				if ($_FILES['foto']['type'] != 'image/jpeg') {
					$json = array(
						'status' => 3,
						'description' => 'el tipo de formato de la foto debe ser .jpg'
						);

					echo json_encode($json);
				} elseif ($_FILES['foto']['size'] > 1024 * 1024) {
					$json = array(
						'status' => 3,
						'description' => 'la foto no debe ser de mas de 1Mb'
						);

					echo json_encode($json);
				} else {
					move_uploaded_file($_FILES['foto']['tmp_name'], '../usuarios/fotos/' . $_POST['usuario'] . '.jpg');
					echo $usuarios->create($_POST['nombre'], $_POST['apellido'], $_POST['usuario'], $_POST['clave'], $_POST['biblioteca'], $_POST['nivel']);
				}
			} else {
				echo $usuarios->create($_POST['nombre'], $_POST['apellido'], $_POST['usuario'], $_POST['clave'], $_POST['biblioteca'], $_POST['nivel']);
			}
		} else {
			$json = array(
				'status' => 0,
				'description' => 'el usuario ya existe!'
				);

			echo json_encode($json);
		}
	} else {
		$json = array(
			'status' => 0,
			'description' => 'las contraseñas no coinciden'
			);

		echo json_encode($json);
	}
} else {
	$json = array(
		'status' => 0,
		'description' => 'algunos campos estan vacios'
		);

	echo json_encode($json);
}
?>