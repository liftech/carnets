<?php
require_once 'libs/autoload.php';
$personas = new Personas();
$carnets = new Carnets();

$idsCarnets = explode(',', $_GET['carnets']);
$ciPersonas = explode(',', $_GET['personas']);

$dataCarnets = array();
$dataPersonas = array();

$data = array();

if (count($idsCarnets) == count($ciPersonas)) {
	$cantCarnets = count($idsCarnets);
	for ($i=0; $i < $cantCarnets; $i++) { 
		array_push($dataPersonas, json_decode($personas->read($ciPersonas[$i])));
	}

	$cantPersonas = count($dataPersonas);

	for ($i=0; $i < $cantPersonas; $i++) { 
		array_push($dataCarnets, json_decode($carnets->read($dataPersonas[$i]->id)));

		array_push($data, array(
			'id' => $dataPersonas[$i]->id,
			'cedula' => $dataPersonas[$i]->cedula,
			'nombre' => $dataPersonas[$i]->nombre,
			'apellido' => $dataPersonas[$i]->apellido,
			'carnet' => json_decode($carnets->read($dataPersonas[$i]->id))
			));
	}
}
$data = json_encode($data);
$data = json_decode($data);
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
	</head>
	<body onload="window.print()">
		<?php for ($i=0; $i < count($data); $i++) { ?>
		<img src="php/img_carnet.php?carnet=<?php echo $data[$i]->carnet->carnet; ?>&cedula=<?php echo $data[$i]->cedula; ?>&nombre=<?php echo $data[$i]->nombre; ?>&apellido=<?php echo $data[$i]->apellido; ?>&&biblioteca=<?php echo $data[$i]->carnet->biblioteca; ?>" alt="">
		<img src="php/img_carnet_atras.php?fecha_exp=<?php echo $data[$i]->carnet->fecha_exp; ?>&fecha_ven=<?php echo $data[$i]->carnet->fecha_ven; ?>" alt="">
		<br />
		<?php } ?>
	</body>
</html>