<?php
require_once 'php/session.php';
require_once 'libs/autoload.php';
require_once 'libs/funciones.php';
$carnets = new Carnets();

$todos = json_decode($carnets->listarCarnets());
$impresos = array();
$porImprimir = array();

for ($i=0; $i < count($todos); $i++) { 
	if ($todos[$i]->impreso == 1) {
		array_push($impresos, $todos[$i]);
	} elseif ($todos[$i]->impreso == 0) {
		array_push($porImprimir, $todos[$i]);
	}
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="plugins/daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
	</head>
	<body class="sidebar-mini skin-red-light">
		<div class="wrapper">
			<div id="logoHead" style="text-align: center;background-color: #fff;">
				<img src="dist/img/Logo2.jpg" width="850" alt="">
			</div>
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar" style="top: 98px;">
				<?php include 'inc/main-aside.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Carnets <small>Lista de carnet</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Carnets</li>
					</ol>
				</section>
				<section class="content">
					<div class="col-md-12">
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#todos" data-toggle="tab">Todos</a></li>
								<li><a href="#impresos" data-toggle="tab">Impresos</a></li>
								<li><a href="#porImprimir" data-toggle="tab">Por Imprimir</a></li>
							</ul>
							<div class="tab-content">
								<div class="active tab-pane" id="todos">
									<table id="tableTodos" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Código</th>
												<th>Cédula</th>
												<th>Apellido</th>
												<th>Nombre</th>
												<th>Fecha de Expedición</th>
												<th>Fecha de Vencimiento</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i < count($todos); $i++) { ?>
											<tr>
												<td><?php echo $todos[$i]->carnet; ?></td>
												<td><?php echo $todos[$i]->cedula; ?></td>
												<td><?php echo $todos[$i]->apellido; ?></td>
												<td><?php echo $todos[$i]->nombre; ?></td>
												<td><?php echo fechas($todos[$i]->fecha_exp); ?></td>
												<td><?php echo fechas($todos[$i]->fecha_ven); ?></td>
												<td>
													<?php echo estados($todos[$i]->estado, 'Activo', 'Vencido'); ?>
													<?php if ($todos[$i]->estado == 0) { ?>
													<button class="btn btn-primary btn-flat btn-xs activar" data-carnet="<?php echo $todos[$i]->id; ?>">Activar</button>
													<?php } ?>
												</td>
											</tr>
											<?php } ?>
										</tbody>
										<tfoot>
										<tr>
											<th>Código</th>
											<th>Cédula</th>
											<th>Apellido</th>
											<th>Nombre</th>
											<th>Fecha de Expedición</th>
											<th>Fecha de Vencimiento</th>
											<th>Estado</th>

										</tr>
										</tfoot>
									</table>
								</div>
								<div class="tab-pane" id="impresos">
									<table id="tableImpresos" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Código</th>
												<th>Cédula</th>
												<th>Apellido</th>
												<th>Nombre</th>
												<th>Fecha de Expedición</th>
												<th>Fecha de Vencimiento</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i < count($impresos); $i++) { ?>
											<tr>
												<td><?php echo $impresos[$i]->carnet; ?></td>
												<td><?php echo $impresos[$i]->cedula; ?></td>
												<td><?php echo $impresos[$i]->apellido; ?></td>
												<td><?php echo $impresos[$i]->nombre; ?></td>
												<td><?php echo fechas($impresos[$i]->fecha_exp); ?></td>
												<td><?php echo fechas($impresos[$i]->fecha_ven); ?></td>
												<td><?php echo estados($impresos[$i]->estado, 'Activo', 'Vencido'); ?></td>
											</tr>
											<?php } ?>
										</tbody>
										<tfoot>
										<tr>
											<th>Código</th>
											<th>Cédula</th>
											<th>Apellido</th>
											<th>Nombre</th>
											<th>Fecha de Expedición</th>
											<th>Fecha de Vencimiento</th>
											<th>Estado</th>

										</tr>
										</tfoot>
									</table>
								</div>
								<div class="tab-pane" id="porImprimir">
									<a id="imprimir" class="btn btn-primary btn-flat" target="_blank">Imprimir</a>
									<table id="tablePorImprimir" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Código</th>
												<th>Cédula</th>
												<th>Apellido</th>
												<th>Nombre</th>
												<th>Fecha de Expedición</th>
												<th>Fecha de Vencimiento</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i < count($porImprimir); $i++) { ?>
											<tr>
												<td><input type="checkbox" data-carnet="<?php echo $porImprimir[$i]->id; ?>" data-persona="<?php echo $porImprimir[$i]->cedula; ?>" class="porImprimir"></td>
												<td><?php echo $porImprimir[$i]->carnet; ?></td>
												<td><?php echo $porImprimir[$i]->cedula; ?></td>
												<td><?php echo $porImprimir[$i]->apellido; ?></td>
												<td><?php echo $porImprimir[$i]->nombre; ?></td>
												<td><?php echo fechas($porImprimir[$i]->fecha_exp); ?></td>
												<td><?php echo fechas($porImprimir[$i]->fecha_ven); ?></td>
												<td><?php echo estados($porImprimir[$i]->estado, 'Activo', 'Vencido'); ?></td>
											</tr>
											<?php } ?>
										</tbody>
										<tfoot>
										<tr>
											<th>#</th>
											<th>Código</th>
											<th>Cédula</th>
											<th>Apellido</th>
											<th>Nombre</th>
											<th>Fecha de Expedición</th>
											<th>Fecha de Vencimiento</th>
											<th>Estado</th>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/daterangepicker/moment.js"></script>
		<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.js"></script>
		<script type="text/javascript" src="dist/js/carnets.js"></script>
		<script type="text/javascript">
		$(function() {
			$("#tableTodos, #tableImpresos, #tablePorImprimir").DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": false,
				"ordering": false,
				"info": true,
				"autoWidth": false
			});
			$('#reservation').daterangepicker();
		});
		</script>
	</body>
</html>