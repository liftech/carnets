-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2016 a las 00:29:05
-- Versión del servidor: 5.6.25
-- Versión de PHP: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carnets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bibliotecas`
--

CREATE TABLE IF NOT EXISTS `bibliotecas` (
  `id` int(2) unsigned NOT NULL,
  `biblioteca` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bibliotecas`
--

INSERT INTO `bibliotecas` (`id`, `biblioteca`) VALUES
(1, 'B. P. Eutimio Rivas'),
(2, 'B. P. Linis Guerrero'),
(3, 'B. P. Héctor Roviro Ruíz'),
(4, 'B. P. Carmen Valverde'),
(5, 'B. P. Julio Cesar Salas'),
(6, 'B. P. San José de Palmira'),
(7, 'B. P. Bachiller Mario Bonilla'),
(8, 'B. P. Don Simón Rodríguez'),
(9, 'B. P. Santa Elena de Arenales'),
(10, 'B. P. Tomas Castelao'),
(11, 'B. P. Independencia'),
(12, 'B. P. José Humberto Paparoni'),
(13, 'B. P. Nectario Hernán Barillas'),
(14, 'B. P. Mesa de las Palmas'),
(15, 'B. P. Balmore Carrero'),
(16, 'B. P. Rivas Dávila'),
(17, 'B. P. José Vicente Escalante'),
(18, 'B. P. Julia Ruíz'),
(19, 'B. P. Las Acacias'),
(20, 'B. P. Emiro Duque Sánchez'),
(21, 'B. P. Carlos Muñoz Oraá'),
(22, 'B. P. Valparaíso'),
(23, 'B. P. Consuelo Navas'),
(24, 'B. P. Andrés Eloy Blanco'),
(25, 'B. P. Manuel Molina'),
(26, 'B. P. Raúl Ramos Giménez'),
(27, 'B. P. Juan Félix Sánchez y Epifania Gil'),
(28, 'B. P. Lic. Alexander Quintero'),
(29, 'B. P. La Toma'),
(30, 'B. P. Luis Alberto Lobo'),
(31, 'B. P. Blanca Julia de Dugarte'),
(32, 'B. P. Hugo Rafael Chávez Frìas'),
(33, 'B. P. Rodolfo Mora'),
(34, 'B. P. Libertador'),
(35, 'B. P. Carmen Rosa Vega'),
(36, 'B. P. El Molino II'),
(37, 'B. P. José Vicente Nucete'),
(38, 'B. P. Rafael Corredor Rojas'),
(39, 'B. P. Ignacio Fernández Peña'),
(40, 'B. P. Padre Duque'),
(41, 'B. P. Magdiel Páez'),
(42, 'B. P. C. Simón Bolívar'),
(43, 'B. P. Mariana Mozart'),
(44, 'B. P. Carabobo'),
(45, 'B. P. Clara Vivas Briceño'),
(46, 'B. P. El Morro'),
(47, 'B. P. San Antonio'),
(48, 'B. P. Generalísimo Francisco de Miranda'),
(49, 'B. P. Bicentenario'),
(50, 'Servicio Móvil Bibliobús'),
(51, 'B. P. El Arañero'),
(52, 'B. P. Presbítero José Ramón Gallegos'),
(53, 'B. P. Antonio Pinto Salinas'),
(54, 'B. P. Teresa de la Parra'),
(55, 'B. P. 19 de Abril'),
(56, 'B. P. Merley Mejías'),
(57, 'B. P. Antonio Ramón Molina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carnets`
--

CREATE TABLE IF NOT EXISTS `carnets` (
  `id` int(10) unsigned NOT NULL,
  `biblioteca` int(11) unsigned NOT NULL,
  `carnet` varchar(10) NOT NULL,
  `fecha_exp` varchar(15) NOT NULL,
  `fecha_ven` varchar(15) NOT NULL,
  `estado` int(1) unsigned NOT NULL COMMENT '0 = vencido; 1 = activo',
  `persona` int(11) unsigned NOT NULL,
  `usuario` int(11) unsigned NOT NULL,
  `impreso` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 = sin imprimir; 1 = impreso'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `carnets`
--

INSERT INTO `carnets` (`id`, `biblioteca`, `carnet`, `fecha_exp`, `fecha_ven`, `estado`, `persona`, `usuario`, `impreso`) VALUES
(1, 1, '1-1', '1464543002', '1496099928', 1, 1, 1, 0),
(2, 1, '1-2', '1464543002', '1496099928', 1, 2, 1, 0),
(3, 1, '1-3', '1464543002', '1496099928', 1, 3, 1, 0),
(4, 4, '4-1', '1464543002', '1496099928', 1, 4, 1, 0),
(5, 1, '1-4', '1464543002', '1496099928', 1, 5, 1, 0),
(6, 1, '1-5', '1464543002', '1496099928', 1, 6, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id` int(10) unsigned NOT NULL,
  `cedula` varchar(15) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nombre`, `apellido`) VALUES
(1, 'V87654321', 'Alejandro', 'Acevedo'),
(2, 'V76543210', 'Carolina', 'Acevedo'),
(3, 'V65432109', 'Zulay', 'Acevedo'),
(4, 'V54321098', 'Ligia', 'Rojas'),
(5, 'V20831818', 'Rafael', 'Moreno'),
(6, 'V20831817', 'Iris', 'Moreno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `clave` varchar(200) NOT NULL,
  `biblioteca` int(11) unsigned NOT NULL,
  `nivel` int(1) unsigned NOT NULL COMMENT '0 = general; 1 = administrador',
  `estado` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 = bloqueado; 1 = activo',
  `intentos_login` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `usuario`, `clave`, `biblioteca`, `nivel`, `estado`, `intentos_login`) VALUES
(1, 'Rafael', 'Moreno', 'rafael', '4098a6c4a7cce9bcf45d9750a1420278', 1, 1, 1, 0),
(2, 'Karina', 'Araque', 'karina', 'da772358105e960864822c7d59a1c0f7', 1, 1, 1, 0),
(3, 'Keyla', 'Moreno', 'keyla', '0561c1120ad0d3d5e8f1aaf334ba550a', 5, 0, 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bibliotecas`
--
ALTER TABLE `bibliotecas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carnets`
--
ALTER TABLE `carnets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bibliotecas`
--
ALTER TABLE `bibliotecas`
  MODIFY `id` int(2) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `carnets`
--
ALTER TABLE `carnets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
