var login = function() {
    $.ajax({
        url: 'php/acceso.php',
        type: 'POST',
        dataType: 'json',
        data: {
            usuario: $('#usuario').val(),
            clave: $('#clave').val(),
        },
        beforeSend: function() {
            console.log('Cargando...')
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                window.location = 'principal.php';
            } else {
            	$('#alertas').html('<div class="callout callout-danger"><h4>Error!</h4><p>' + data.description + '</p></div>');
            	$('input').val('');
            	$('#usuario').focus();
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
            $('#alertas').html('<div class="callout callout-danger"><h4>Error!</h4><p>' + err.responseText + '</p></div>');
        }
    });
}

$('#entrar').click(login);

$('#formLogin').keypress(function(event) {
    if (event.keyCode == 13) {
        login();
    };
});