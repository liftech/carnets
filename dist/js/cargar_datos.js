$('#cargarExcel').click(function() {
    var formData = new FormData($('#formulario')[0]);
    $.ajax({
        url: 'php/cargar_datos.php',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
            console.log('Cargando...')
        },
        success: function(data) {
            console.log(data);
            for (var i = 0; i < data.exitos.length; i++) {
                $('#exitos').append('<tr><td>' + data.exitos[i].cedula + '</td><td>' + data.exitos[i].nombre + '</td><td>' + data.exitos[i].apellido + '</td></tr>');
            };

            for (var i = 0; i < data.errores.length; i++) {
                $('#errores').append('<tr><td>' + data.errores[i].cedula + '</td><td>' + data.errores[i].nombre + '</td><td>' + data.errores[i].apellido + '</td><td>' + data.errores[i].errors + '</td></tr>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });
});