$('#agregar').click(function() {
    var formData = new FormData($('#formulario')[0]);
    $.ajax({
        url: 'php/usuarios.php',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
            console.log('Cargando...')
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                $('input').val('');
                $('#alertas').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Exito!</h4>El usuario se agregar satiscatoriamente.</div>');
            } else {
                $('#alertas').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4>' + data.description + '</div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });
});