var addBiblioteca = function() {
    $.ajax({
        url: 'php/bibliotecas.php',
        type: 'POST',
        dataType: 'json',
        data: {
            nombre: $('#nombre').val()
        },
        beforeSend: function() {
            console.log('Cargando...')
        },
        success: function(data) {
            console.log(data);
            alert(data.description);
            $('#nombre').val('');
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });
}

$('#agregar').click(addBiblioteca);

$('#nombre').keypress(function(event) {
    if (event.keyCode == 13) {
    	addBiblioteca();
    };
});