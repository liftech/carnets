$('#cedula, #nombre, #apellido').keyup(function() {
    $.post('php/carnet_img.php', {
        cedula: $('#cedula').val(),
        nombre: $('#nombre').val(),
        apellido: $('#apellido').val()
    }, function(data, textStatus, xhr) {
        $('#preview').html(data);
    });
});

$('#cedula').blur(function(event) {
    $.post('php/carnets.php', {
        cedula: $(this).val(),
        accion: 'consulta'
    }, function(data, textStatus, xhr) {
        var json = $.parseJSON(data);
        console.log(json);
        if (!$.isEmptyObject(json)) {
            if (json.status != 1) {
                $('#nombre').prop('disabled', true);
                $('#apellido').prop('disabled', true);
                $('#agregar').prop('disabled', true);
                $('#alertaCedula').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + json.description + '</div>');
            } else {
                $('#nombre').prop('disabled', false);
                $('#apellido').prop('disabled', false);
                $('#agregar').prop('disabled', false);
                $('#alertaCedula').html('');
            };
        };
    });
});

$('#agregar').click(function() {
    $.ajax({
        url: 'php/carnets.php',
        type: 'POST',
        dataType: 'json',
        data: {
            cedula: $('#cedula').val(),
            nombre: $('#nombre').val(),
            apellido: $('#apellido').val(),
            biblioteca: $('#biblioteca').val(),
            accion: 'agregar'
        },
        beforeSend: function() {
            console.log('Cargando...')
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                $('input').val('');
                $('#cedula').focus();
                $('#alertas').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Exito!</h4>' + data.description + '.</div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
        }
    });
});

var carnetsImp = [];
var personasImp = [];
$('.porImprimir').click(function() {
    if ($(this).prop('checked') == true) {
        var indexCarnet = carnetsImp.indexOf($(this).data('carnet'));
        var indexPersona = personasImp.indexOf($(this).data('persona'));
        if (indexCarnet < 0 && indexPersona < 0) {
            carnetsImp.push($(this).data('carnet'));
            personasImp.push($(this).data('persona'));
            console.log(carnetsImp);
            console.log(personasImp);
            $('#imprimir').prop('href', 'imprimir_carnets.php?carnets=' + carnetsImp.toString() + '&personas=' + personasImp.toString());
        };
    } else {
        var indexCarnet = carnetsImp.indexOf($(this).data('carnet'));
        var indexPersona = personasImp.indexOf($(this).data('persona'));
        if (indexCarnet >= 0 && indexPersona >= 0) {
            carnetsImp.splice(indexCarnet, 1);
            personasImp.splice(indexPersona, 1);
            console.log(carnetsImp);
            console.log(personasImp);
            $('#imprimir').prop('href', 'imprimir_carnets.php?carnets=' + carnetsImp.toString() + '&personas=' + personasImp.toString());
        };
    };
});

$('.activar').click(function() {
    $.post('php/carnets.php', {
        id: $(this).data('carnet'),
        accion: 'activar'
    }, function(data, textStatus, xhr) {
        var json = $.parseJSON(data);
        console.log(json);
        if (json.status == 1) {
            alert(json.description);
            location.reload();
        };
    });
});

/*
$('#imprimir').click(function() {
    $.ajax({
        url: 'php/imprimir_carnets.php',
        type: 'POST',
        dataType: 'html',
        data: {
            carnetsImp: carnetsImp
        },
    })
        .done(function(data) {
            console.log(data);
        })
        .fail(function(err) {
            console.log(err);
        })
        .always(function() {
            console.log("complete");
        });
});
*/