<?php
require_once 'php/session.php';
require_once 'libs/autoload.php';
$bi = new Bibliotecas();
$lista_bib = json_decode($bi->listarBibliotecas());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/select2/select2.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
	</head>
	<body class="sidebar-mini skin-red-light">
		<div class="wrapper">
			<div id="logoHead" style="text-align: center;background-color: #fff;">
				<img src="dist/img/Logo2.jpg" width="850" alt="">
			</div>
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar" style="top: 98px;">
				<?php include 'inc/main-aside.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Usuarios <small>Crear usuario</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">usuarios</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Ingrese los siguientes datos</h3>
								</div>
								<form id="formulario" action="php/usuarios.php" method="POST" autocomplete="off">
									<div class="box-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="nombre">Nombre</label>
													<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ejm: Pedro">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="apellido">Apellido</label>
													<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Ejm: Pérez">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="biblioteca">Biblioteca</label>
											<select name="biblioteca" id="biblioteca" class="form-control select2" style="width: 100%;">
												<option value="">Seleccione:</option>
												<?php for ($i=0; $i < count($lista_bib); $i++) { ?>
												<option value="<?php echo $lista_bib[$i]->id; ?>"><?php echo $lista_bib[$i]->biblioteca; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group">
											<label for="nivel">Nivel de Acceso</label>
											<select name="nivel" id="nivel" class="form-control select2" style="width: 100%;">
												<option value="">Seleccione:</option>
												<option value="0">General</option>
												<option value="1">Administrador</option>
											</select>
										</div>
										<div class="form-group">
											<label for="usuario">Usuario</label>
											<input type="text" id="usuario" name="usuario" class="form-control" placeholder="Ejm: pedro">
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="clave">Contraseña</label>
													<input type="password" id="clave" name="clave" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="clave_r">Repita la contraseña</label>
													<input type="password" id="clave_r" name="clave_r" class="form-control">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="foto">Foto (No es obligatorio)</label>
											<input type="file" id="foto" name="foto" class="form-control">
										</div>
										<div id="alertas">
										</div>
									</div>
									<div class="box-footer">
										<button type="button" id="agregar" class="btn btn-primary btn-flat">Crear Usuario</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.js"></script>
		<script type="text/javascript" src="dist/js/usuarios.js"></script>
		<script type="text/javascript">
		$(function() {
			$(".select2").select2();
		});
		</script>
	</body>
</html>