<?php
require_once 'Conexion.php';
/**
* 
*/
class Acceso extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public function login($_usuario, $_clave) {
		$json = array();
		$intentos = 5;

		$_usuario = htmlspecialchars($_usuario, ENT_QUOTES);
		$_clave = md5($_clave);

		$query = "SELECT * FROM `usuarios` WHERE `usuario` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('s', $_usuario);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $apellido, $usuario, $clave, $biblioteca, $nivel, $estado, $intentos_login);
			$stmt->fetch();

			if ($estado == 1) {
				if ($_usuario == $usuario && $clave == $_clave) {
					session_start();
					$_SESSION['id'] = $id;
					$_SESSION['nombre'] = $nombre . " " . $apellido;
					$_SESSION['biblioteca'] = $biblioteca;
					$_SESSION['nivel'] = $nivel;
					$_SESSION['vida'] = time() + 300;

					if (file_exists('../usuarios/fotos/' . $usuario . '.jpg')) {
						$_SESSION['foto'] = 'http://' . $_SERVER['HTTP_HOST'] . '/carnets/usuarios/fotos/' . $usuario . '.jpg';
					} else {
						$_SESSION['foto'] = 'http://' . $_SERVER['HTTP_HOST'] . '/carnets/dist/img/default.jpg';
					}

					$update = "UPDATE `usuarios` SET `intentos_login`= 0 WHERE `usuario` = ?";
					if ($stmt2 = $this->_mysqli->prepare($update)) {
						$stmt2->bind_param('s', $_usuario);
						$stmt2->execute();
					}

					$json = array(
						"status" => 1,
						"description" => "login correcto"
						);
				} else {
					$_intentos_login = $intentos_login + 1;
					if ($_intentos_login >= 5) {
						$update = "UPDATE `usuarios` SET `estado`= 0, `intentos_login`= 0 WHERE `usuario` = ?";
						if ($stmt2 = $this->_mysqli->prepare($update)) {
							$stmt2->bind_param('s', $_usuario);
							$stmt2->execute();
						}
						$json = array(
							"status" => 0,
							"description" => "error en los datos, el usuario se ha bloqueado. Comuniquese con el administrador"
							);
					} else {
						$update = "UPDATE `usuarios` SET `intentos_login`= ? WHERE `usuario` = ?";
						if ($stmt2 = $this->_mysqli->prepare($update)) {
							$stmt2->bind_param('is', $_intentos_login, $_usuario);
							$stmt2->execute();
						}
						if ($_intentos_login >= 2) {
							$json = array(
								"status" => 0,
								"description" => "error en los datos. Si olvido sus datos comuniquese con el administrador"
								);
						} else {
							$json = array(
							"status" => 0,
							"description" => "error en los datos"
							);
						}
					}
				}
			} elseif ($estado == 0) {
				$json = array(
					"status" => 0,
					"description" => "el usuario esta bloqueado. Comuniquese con el administrador"
					);
			}
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>