<?php
require_once 'Conexion.php';
/**
* 
*/
class Bibliotecas extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function create($nombre) {
		$json = array();

		$nombre = htmlspecialchars($nombre, ENT_QUOTES);

		$query = "INSERT INTO `bibliotecas`(`id`, `biblioteca`) VALUES (NULL, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('s', $nombre);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "la biblioteca se creo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function read($_id) {
		$json = array();
		$query = "SELECT * FROM `bibliotecas` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $_id);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $biblioteca);
			$stmt->fetch();

			$json = array(
				'id' => $id,
				'biblioteca' => $biblioteca
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function update($id, $biblioteca) {
		$json = array();

		$biblioteca = htmlspecialchars($biblioteca, ENT_QUOTES);

		$query = "UPDATE `bibliotecas` SET `biblioteca` = ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('si', $biblioteca, $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "la biblioteca se actualizo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function delete($id) {
		$json = array();

		$query = "DELETE FROM `bibliotecas` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "la biblioteca se elimino satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/

	public function listarBibliotecas() {
		$json = array();
		$query = "SELECT * FROM `bibliotecas`";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $biblioteca);
			while ($stmt->fetch()) {
				array_push($json, array(
					'id' => $id,
					'biblioteca' => $biblioteca
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}

	public function buscarPor($nombre) {
		$json = array();
		
		$nombre = htmlspecialchars($nombre, ENT_QUOTES);

		$query = "SELECT * FROM `bibliotecas` WHERE `biblioteca` LIKE '%" . $nombre . "%'";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $biblioteca);
			$stmt->fetch();

			if ($stmt->num_rows > 0) {
				$json = array(
					'id' => $id,
					'biblioteca' => $biblioteca,
					'registros' => $stmt->num_rows
					);
			} else {
				$json = array(
					'status' => 0,
					'description' => "no se encontro la bibliotca",
					'registros' => $stmt->num_rows
					);
			}
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>