<?php
require_once 'Conexion.php';
/**
* 
*/
class Estadisticas extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public function recordBibliotecas() {
		$json = array();
		$query = "SELECT `bibliotecas`.*, COUNT(`carnets`.`id`) AS `cantidad` FROM `bibliotecas`, `carnets` WHERE `bibliotecas`.`id` = `carnets`.`biblioteca` GROUP BY `id` ORDER BY `cantidad` DESC LIMIT 5";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $biblioteca, $cantidad);
			while ($stmt->fetch()) {
				array_push($json, array(
					'id' => $id,
					'biblioteca' => $biblioteca,
					'cantidad' => $cantidad
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>