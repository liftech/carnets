<?php
/**
* 
*/
class Conexion {

	protected $_mysqli;
	
	function __construct() {
		$this->_mysqli = new mysqli("localhost", "root", "", "carnets");
		if ($this->_mysqli->connect_error) {
			die('Error de Conexión (' . $this->_mysqli->connect_errno . ') ' . $this->_mysqli->connect_error);
		}
		$this->_mysqli->set_charset("utf8");
	}
}
?>