<?php
require_once 'Conexion.php';
/**
* 
*/
class Personas extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function create($cedula, $nombre, $apellido) {
		$json = array();

		$cedula = htmlspecialchars($cedula, ENT_QUOTES);
		$nombre = htmlspecialchars($nombre, ENT_QUOTES);
		$apellido = htmlspecialchars($apellido, ENT_QUOTES);

		$query = "INSERT INTO `personas` VALUES (NULL, ?, ?, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('sss', $cedula, $nombre, $apellido);
			$stmt->execute();

			$json = array(
				"id" => $this->_mysqli->insert_id,
				"status" => 1,
				"description" => "el carnet se creo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function read($_cedula) {
		$json = array();

		$_cedula = htmlspecialchars($_cedula, ENT_QUOTES);

		$query = "SELECT * FROM `personas` WHERE `cedula` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('s', $_cedula);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $cedula, $nombre, $apellido);
			$stmt->fetch();

			$json = array(
				'id' => $id,
				'cedula' => $cedula,
				'nombre' => $nombre,
				'apellido' => $apellido
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/

	public function listarPersonas() {
		$json = array();

		$query = "SELECT * FROM `personas`";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $cedula, $nombre, $apellido);
			while ($stmt->fetch()) {
				array_push($json, array(
					'id' => $id,
					'cedula' => $cedula,
					'nombre' => $nombre,
					'apellido' => $apellido
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>