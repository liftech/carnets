<?php
require_once 'Conexion.php';
/**
* 
*/
class Carnets extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function create($biblioteca, $persona, $usuario) {
		$json = array();

		$fecha_exp = time();
		$fecha_ven = $fecha_exp + 31556926;

		$query = "SELECT COUNT(`carnet`) AS `carnets` FROM `carnets` WHERE `biblioteca` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $biblioteca);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($carnets);
			$stmt->fetch();

			$carnet = $biblioteca . '-' . ($carnets + 1);
			//$insert = "INSERT INTO `carnets` VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, 1, ?, 0)";
			$insert = "INSERT INTO `carnets` VALUES (NULL, ?, ?, ?, ?, 1, ?, ?, 0)";
			if ($stmt = $this->_mysqli->prepare($insert)) {
				$stmt->bind_param('isssii', $biblioteca, $carnet, $fecha_exp, $fecha_ven, $persona, $usuario);
				$stmt->execute();

				$json = array(
					"status" => 1,
					"description" => "el carnet se creo satisfactoriamente"
					);
			}
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function read($_persona) {
		$json = array();
		$query = "SELECT * FROM `carnets` WHERE `persona` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $_persona);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $biblioteca, $carnet, $fecha_exp, $fecha_ven, $estado, $persona, $usuario, $impreso);
			$stmt->fetch();

			$json = array(
				'id' => $id,
				'biblioteca' => $biblioteca,
				'carnet' => $carnet,
				'fecha_exp' => $fecha_exp,
				'fecha_ven' => $fecha_ven,
				'estado' => $estado,
				'persona' => $persona,
				'usuario' => $usuario,
				'impreso' => $impreso
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function update($id, $cedula, $nombre, $apellido, $biblioteca, $carnet, $fecha_exp, $fecha_ven, $estado, $usuario) {
		$json = array();

		$cedula = htmlspecialchars($cedula, ENT_QUOTES);
		$nombre = htmlspecialchars($nombre, ENT_QUOTES);
		$apellido = htmlspecialchars($apellido, ENT_QUOTES);

		$query = "UPDATE `carnets` SET `cedula` = ?, `nombre` = ?, `apellido` = ?, `biblioteca` = ?, `carnet` = ?, `fecha_exp` = ?, `fecha_ven` = ?, `estado` = ?, `usuario` = ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('sssisssiii', $cedula, $nombre, $apellido, $biblioteca, $carnet, $fecha_exp, $fecha_ven, $estado, $usuario, $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el carnet se actualizo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function delete($id) {
		$json = array();

		$query = "DELETE FROM `carnets` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el carnet se elimino satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/

	public function ultimoCarnet() {
		$query = "SELECT `id` FROM `carnets` ORDER BY `id` DESC LIMIT 1";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id);
			$stmt->fetch();

			$ultimo = $id;
		}
		$stmt->close();
		return $ultimo;
	}


	public function listarCarnets() {
		$json = array();

		$query = "SELECT `personas`.`cedula`, `personas`.`nombre`, `personas`.`apellido`, `carnets`.*  FROM `carnets`, `personas` WHERE `carnets`.`persona` = `personas`.`id` ORDER BY `carnets`.`fecha_exp` DESC";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($cedula, $nombre, $apellido, $id, $biblioteca, $carnet, $fecha_exp, $fecha_ven, $estado, $persona, $usuario, $impreso);
			while ($stmt->fetch()) {
				array_push($json, array(
					'cedula' => $cedula,
					'nombre' => $nombre,
					'apellido' => $apellido,
					'id' => $id,
					'biblioteca' => $biblioteca,
					'carnet' => $carnet,
					'fecha_exp' => $fecha_exp,
					'fecha_ven' => $fecha_ven,
					'estado' => $estado,
					'persona' => $persona,
					'usuario' => $usuario,
					'impreso' => $impreso
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}

	private function rangosFechas($rangos) {
		$rangos = str_replace(' ', '', $rangos);

		$fechas = explode('-', $rangos);
		$desde = $fechas[0];
		$hasta = $fechas[1];

		return array(
			'desde' => strtotime($desde . ' 00:00:00'),
			'hasta' => strtotime($hasta . ' 23:59:59')
			);
	}

	public function activar($id) {
		$json = array();

		$fecha_exp = time();
		$fecha_ven = $fecha_exp + 31556926;

		$query = "UPDATE `carnets` SET `fecha_exp`= ?, `fecha_ven`= ?, `estado`= 1 WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('ssi', $fecha_exp, $fecha_ven, $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el carnet se activo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>