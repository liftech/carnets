<?php
require_once 'Conexion.php';
/**
* 
*/
class Usuarios extends Conexion {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function create($nombre, $apellido, $usuario, $clave, $biblioteca, $nivel) {
		$json = array();

		$nombre = htmlspecialchars($nombre, ENT_QUOTES);
		$apellido = htmlspecialchars($apellido, ENT_QUOTES);
		$usuario = htmlspecialchars($usuario, ENT_QUOTES);
		$clave = md5($clave);

		$query = "INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `usuario`, `clave`, `biblioteca`, `nivel`) VALUES (NULL, ?, ?, ?, ?, ?, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('ssssii', $nombre, $apellido, $usuario, $clave, $biblioteca, $nivel);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el usuario se creo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function read($id) {
		$json = array();

		$query = "SELECT `id`, `nombre`, `apellido`, `usuario`, `biblioteca`, `nivel` FROM `usuarios` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $apellido, $usuario, $biblioteca, $nivel);
			$stmt->fetch();

			$json = array(
				'id' => $id,
				'nombre' => $nombre,
				'apellido' => $apellido,
				'usuario' => $usuario,
				'biblioteca' => $biblioteca,
				'nivel' => $nivel
				);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function update($id, $nombre, $apellido, $usuario, $clave, $biblioteca, $nivel) {
		$json = array();

		$nombre = htmlspecialchars($nombre, ENT_QUOTES);
		$apellido = htmlspecialchars($apellido, ENT_QUOTES);
		$usuario = htmlspecialchars($usuario, ENT_QUOTES);
		$clave = md5($clave);

		$query = "UPDATE `usuarios` SET `nombre` = ?, `apellido` = ?, `usuario` = ?, `clave` = ?, `biblioteca` = ?, `nivel` = ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('ssssiii', $nombre, $apellido, $usuario, $clave, $biblioteca, $nivel, $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el usuario se actualizo satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}

	public function delete($id) {
		$json = array();

		$query = "DELETE FROM `usuarios` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $id);
			$stmt->execute();

			$json = array(
				"status" => 1,
				"description" => "el usuario se elimino satisfactoriamente"
				);
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}
		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/

	public function listarUsuarios() {
		$json = array();
		$query = "SELECT `id`, `nombre`, `apellido`, `usuario`, `biblioteca`, `nivel`, `estado` FROM `usuarios`";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $apellido, $usuario, $biblioteca, $nivel, $estado);
			while ($stmt->fetch()) {
				array_push($json, array(
					'id' => $id,
					'nombre' => $nombre,
					'apellido' => $apellido,
					'usuario' => $usuario,
					'biblioteca' => $biblioteca,
					'nivel' => $nivel,
					'estado' => $estado
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>