<?php
function rangosFechas($rangos) {
	$rangos = str_replace(' ', '', $rangos);

	$fechas = explode('-', $rangos);
	$desde = $fechas[0];
	$hasta = $fechas[1];

	return array(
		'desde' => strtotime($desde . ' 00:00:00'),
		'hasta' => strtotime($hasta . ' 23:59:59')
		);
}

function estados($estado, $exito, $error) {
	if ($estado == 1) {
		return '<span class="label label-success">' . $exito . '</span>';
	} elseif ($estado == 0) {
		return '<span class="label label-danger">' . $error . '</span>';
	}
}

function biblioteca($id) {
	global $bi;
	$data = json_decode($bi->read($id));
	return $data->biblioteca;
}

function niveles($nivel) {
	if ($nivel == 1) {
		return 'Administrador';
	} else {
		return 'General';
	}
}

function fechas($fecha) {
	return date('d/m/Y', $fecha);
}
?>