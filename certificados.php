<?php
header("Content-type: image/jpeg");

$im = imagecreatefromjpeg("dist/img/certificado1.jpg");

$biblioteca = "B. P. Biblioteca";
$codigo = "0-0";
$cedula = "V12345678";
$nombre = "Apellido Nombre";


$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 7 * strlen($biblioteca)) / 2;
imagestring($im, 3, $px, 5, $biblioteca, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 10 * strlen($codigo)) / 2;
imagestring($im, 5, $px, 18, $codigo, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 1 * strlen($cedula)) / 4;
imagestring($im, 5, $px, 109, $cedula, $negro);

$negro = imagecolorallocate($im, 0, 0, 0);
$px = (imagesx($im) - 8.6 * strlen($nombre)) / 5;
imagestring($im, 5, $px, 155, $nombre, $negro);

imagejpeg($im);
imagedestroy($im);
?>