<?php
require_once 'php/session.php';
require_once 'libs/autoload.php';
$list = new Bibliotecas();
$lista = json_decode($list->listarBibliotecas());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
	</head>
	<body class="sidebar-mini skin-red-light">
		<div class="wrapper">
			<div id="logoHead" style="text-align: center;background-color: #fff;">
				<img src="dist/img/Logo2.jpg" width="850" alt="">
			</div>
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar" style="top: 98px;">
				<?php include 'inc/main-aside.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Bibliotecas <small>Lista de Bibliotecas</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Bibliotecas</li>
					</ol>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title"><!--Data Table With Full Features--></h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-3">
									<div>
										<div class="input-group">
											<input type="text" name="nombre" id="nombre" class="form-control" placeholder="nombre del a biblioteca a registrar">
											<span class="input-group-btn">
												<button id="agregar" class="btn btn-primary btn-flat" type="button">Agregar</button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<table id="bibliotecas" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>ID</th>
										<th>Biblioteca</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i=0; $i < count($lista); $i++) { ?>
									<tr>
										<td><?php echo $lista[$i]->id; ?></td>
										<td><?php echo $lista[$i]->biblioteca; ?></td>
									</tr>
									<?php } ?>
								</tbody>
								<tfoot>
								<tr>
									<th>ID</th>
									<th>Biblioteca</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.js"></script>
		<script type="text/javascript" src="dist/js/bibliotecas.js"></script>
		<script type="text/javascript">
		$(function() {
			$("#bibliotecas").DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
			});
		});
		</script>
	</body>
</html>