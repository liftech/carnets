<?php
require_once 'php/session.php';
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
	</head>
	<body class="sidebar-mini skin-red-light">
		<div class="wrapper">
			<div id="logoHead" style="text-align: center;background-color: #fff;">
				<img src="dist/img/Logo2.jpg" width="850" alt="">
			</div>
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar" style="top: 98px;">
				<?php include 'inc/main-aside.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Dashboard <small>Control panel</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-danger">
								<div class="box-header with-border">
									<h3 class="box-title">Bibliotecas con mas carnets (5 max)</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
										<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-md-8">
											<div class="chart-responsive">
												<canvas id="pieChart" style="height:250px"></canvas>
											</div>
										</div>
										<div id="bibliotecas" class="col-md-4">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/chartjs/Chart.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.js"></script>
		<script type="text/javascript" src="dist/js/estadisticas.js"></script>
	</body>
</html>